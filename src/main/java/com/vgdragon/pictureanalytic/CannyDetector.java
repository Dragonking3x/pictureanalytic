package com.vgdragon.pictureanalytic;


import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class CannyDetector {


    public void cannyDetector () {
        String oriImg = "C:\\Users\\Dragonking3x\\Pictures\\Camera Roll\\test.jpg";
        String dstImg = "C:\\Users\\Dragonking3x\\Pictures\\Camera Roll\\test-out.jpg";
        int threshold = 2;
        //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        final Mat img = Imgcodecs.imread(oriImg);
        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
        //
        Imgproc.Canny(img, img, threshold, threshold * 3, 3, true);
        //
        Imgcodecs.imwrite(dstImg, img);
    }


}
